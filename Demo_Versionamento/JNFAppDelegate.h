//
//  JNFAppDelegate.h
//  Demo_Versionamento
//
//  Created by Jonathan Nobre on 29/07/14.
//  Copyright (c) 2014 Jonathan Nobre Ferreira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JNFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
