//
//  main.m
//  Demo_Versionamento
//
//  Created by Jonathan Nobre on 29/07/14.
//  Copyright (c) 2014 Jonathan Nobre Ferreira. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JNFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JNFAppDelegate class]));
    }
}
