//
//  JNFDetailViewController.h
//  Demo_Versionamento
//
//  Created by Jonathan Nobre on 29/07/14.
//  Copyright (c) 2014 Jonathan Nobre Ferreira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JNFDetailViewController : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@property (nonatomic) NSArray *array;
@end
