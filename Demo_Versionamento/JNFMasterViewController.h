//
//  JNFMasterViewController.h
//  Demo_Versionamento
//
//  Created by Jonathan Nobre on 29/07/14.
//  Copyright (c) 2014 Jonathan Nobre Ferreira. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JNFDetailViewController;

#import <CoreData/CoreData.h>

@interface JNFMasterViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) JNFDetailViewController *detailViewController;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
